#!/bin/bash

sudo adduser contaazul 

PASSWD="keOnyLN##olacontaazul##FxFN6iBl)apHc" 

sudo echo ${PASSWD} | passwd --stdin contaazul

echo "contaazul ALL=(ALL)  NOPASSWD:ALL" >> /etc/sudoers

sudo sed -i '/PasswordAuthentication yes/s/^#//' /etc/ssh/sshd_config

sudo sed -i "s/PasswordAuthentication no/#PasswordAuthentication no/g" /etc/ssh/sshd_config

sudo service sshd restart

if egrep -i '(debian|ubuntu)' /etc/*-release >/dev/null
then
    echo "Baseado em Debian - APT" > /tmp/userdata_status
    sudo apt-get -y update && sudo apt-get -y install git python3 python3-pip vim
    sudo rm /usr/bin/python && sudo /bin/ln -s python3 /usr/bin/python
    ( cd /opt ; git clone https://cezarfw@bitbucket.org/cezarfw/app-python.git ; chmod +x app-python/app.py )
    pip3 install botocore boto3
    python3 /opt/app-python/app.py

elif egrep -i '(centos|redhat)' /etc/*-release >/dev/null
then
    echo "Baseado em RedHat" > /tmp/userdata_status
    sudo yum -y install git python3 python3-pip vim
    sudo rm /usr/bin/python && sudo /bin/ln -s python3 /usr/bin/python
    ( cd /opt ; git clone https://cezarfw@bitbucket.org/cezarfw/app-python.git ; chmod +x app-python/app.py )
    pip3 install botocore boto3
    python3 /opt/app-python/app.py
else
    echo "Distribuição não eh suportada" > /tmp/userdata_status ; exit 1 
fi

echo "Success!" >> /tmp/userdata_status